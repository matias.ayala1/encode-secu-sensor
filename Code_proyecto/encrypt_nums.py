import random
import codecs
from time import localtime, asctime 
import time


def text_to_int(text):
    enc_text = text.encode('utf-8') 
    hex_text = enc_text.hex()
    text_to_int = int(hex_text,16)
    return text_to_int


def int_to_text(num):
    int_to_hex = hex(num)
    hexa = int_to_hex[2:]
    return codecs.decode(codecs.decode(hexa,'hex'),'ascii')


 