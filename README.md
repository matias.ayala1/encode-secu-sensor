## Encode secu-sensor

Proyecto de criptografia y seguridad de la información TEL252 realizado por:

    -Matias Ayala Nanjari
    -Ignacio Arata Salinas
    -Ariel Bravo Ponce
    -Sergio Rojas Escanilla

El proyecto examina el problema planteado por ACME. La preocupante pérdida de datos y el hecho de que se envían en texto sin formato nos impulsa a encontrar una solución de bajo costo que se adapte a la capacidad del sensor que recopila las temperaturas y que permita enviarlas de forma cifrada al servidor encargado de procesar la información.
Se decide utilizar curvas elípticas basadas en ElGamal para el cifrado ya que las curvas elípticas son un tipo de curva matemática que se utilizan en criptografía para crear sistemas de clave pública. Estos sistemas se utilizan para cifrar y descifrar mensajes de manera segura en Internet. El sistema de clave pública ElGamal es uno de los muchos sistemas de clave pública que utilizan curvas elípticas como base. Además, Para mejorar la transmisión de datos meteorológicos, examinamos cómo el sensor envía los datos a un servidor a través de una nube de AWS. Esto nos permitió desarrollar un método seguro y confiable para la transmisión de datos meteorológicos.

En el repositorio hay 2 carpetas:
"Code_proyecto" y "Code_Servidor_AWS".

Code_proyecto contiene los archivos:

    -encrypt_nums
    -Proyecto
    -server

En el cual encrypt_nums manda las temperaturas encriptadas al servidor AWS y retorna las temperaturas encriptadas.

server simula la parte del servidor, en el cual se encarga de desencriptar las temperaturas de forma local.

Code_Servidor_AWS contiene los siguientes archivos:

    -decrypt
    -lambda_function

decrypt contiene todos los archivos necesarios para poder realizar la desencriptación.

lambda_funciton se encarga de desencriptar las temperaturas enviadas al servidor.


