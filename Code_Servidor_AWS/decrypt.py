import random
import codecs
import json



def text_to_int(text):
    enc_text = text.encode('utf-8') 
    hex_text = enc_text.hex()
    text_to_int = int(hex_text,16)
    return text_to_int


def int_to_text(num):
    int_to_hex = hex(num)
    hexa = int_to_hex[2:]
    return codecs.decode(codecs.decode(hexa,'hex'),'ascii')
    
    
######

def num_to_bin(n):
    return list(map(int,list(format(n, "b"))))


def point_add(P,Q,a,mod):
    if P == (0,0) and Q != (0,0):
        X3,Y3 = Q
    if P[0] == Q[0] : #Si xp = xq 
        if P[1] == mod-Q[1] :
            X3,Y3 = 0,0
        elif P[1] == Q[1] and P[1] != 0 and Q[1] != 0:
            X3,Y3 = 0,0
        elif P == Q == (0,0):
            X3,Y3 = 0,0
    else: 
        S = (Q[1]-P[1]) * pow(Q[0]-P[0],-1,mod) % mod  #Calculamos el inverso con pow, S = y2-y1/x2-x1
        X3 = (pow(S,2)-P[0]-Q[0]) % mod  #X3 = S^2-X1-X2 mod p
        Y3 = (S*(P[0]-X3)-P[1]) % mod #Y3 = S(X1-X3) -X2 mod p
    return X3,Y3




def point_dup(P,a,mod):
    if P[0] == 0 and P[1] == 0:
        X3,Y3 = 0,0
    elif P[1] == 0:
        X3,Y3 = 0,0
    else:
        S = ((3*pow(P[0],2)+a) * pow(2*P[1],-1,mod)) %mod # S= 3x1^2+a/2y1
        X3 = (pow(S,2)-P[0]-P[0]) % mod #X3 = S^2-X1-X2 mod p
        Y3 = (S*(P[0]-X3)-P[1]) % mod #Y3 = S(X1-X3) -X2 mod p
    return X3,Y3


def montgomery_ladder(a,mod,n,p):
    R0= p
    R1 = point_dup(p,a,mod)
    n_bin = num_to_bin(n)
    for i in n_bin[1:]: #Nos saltamos el primer 1
        if i == 1:
            R0 = point_add(R1,R0,a,mod)
            R1 = point_dup(R1,a,mod)
        else:
            R1 = point_add(R1,R0,a,mod)
            R0 = point_dup(R0,a,mod)
    return R0


#########
def decrypt_ECELGamal(mod,a,y,r,Q):
    rQ = montgomery_ladder(a,mod,r,Q)
    m = y*pow(rQ[0],-1,mod) %mod
    return m